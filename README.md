# README

This README would normally document whatever steps are necessary to get the
application up and running.

- Ruby version - 2.6.5

- System dependencies

- Configuration

- Database creation
  crear base de datos en motor y luego configurar la base de datos en el archivo config/database.yml
  database: rails o el nombre que usted utilice.
  username: usuario del motor de base de datos.
  password: password del motor de base de datos
  host: localhost
  port: puerto de la base de datos

- Database initialization
  rails db:migrate
